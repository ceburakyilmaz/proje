class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.string :sector
      t.integer :price
      t.string :y_o_production
      t.references :trademark

      t.timestamps
    end
  end
end
