class CreateTrademarks < ActiveRecord::Migration[5.2]
  def change
    create_table :trademarks do |t|
      t.string :name
      t.text :description
      t.string :origin
      t.string :web

      t.timestamps
    end
  end
end
