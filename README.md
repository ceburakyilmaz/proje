# README

### Bu proje, iwallet şirketi teknik mülakatı için hazırlanmıştır.

**Proje, Ruby on Rails ile hazırlanmıştır.**

Proje basit bir Marka-Ürün Listeleme uygulamasıdır. 

- Uygulama içerisinden, Marka ve Ürün oluşturma, düzenleme, güncelleme ve silme işlemleri(CRUD) yapılabilir.

- Uygulama temel işlemlerini tamamladıktan sonra, uygulama içinde her markanın ürünleri listelenebilmektedir.

**Uygulamada kullanılan Ek Gem'ler**

| Gem | Açıklama |
| ------ | ------ |
| devise | Kullanıcı işlemleri için |
| bootstrap - haml | Views sayfaları için |
| simple_form | Form işlemleri için |