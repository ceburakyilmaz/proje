class Product < ApplicationRecord
  belongs_to :trademark

  validates :trademark_id, numericality: { only_integer: true, less_than: 9223372036854775807 }

  validates :name, presence: true, length: { minimum:3, maximum:50, too_short: "için en az %{count} karakter girilmelidir.", too_long: "için en fazla %{count} karakter girilebilir." }
  validates :sector, presence: true
  validates :price, presence: true, numericality: { only_integer: true, less_than: 2147483647 }
  validates :y_o_production, presence: true, length: {is: 4}, numericality: { only_integer: true, greater_than: 1949, less_than_or_equal_to: Time.now.year }
end
