class Trademark < ApplicationRecord
    has_one_attached :image
    has_many :products, dependent: :destroy

    validates :name, length: { minimum:3, maximum:50, too_short: "için en az %{count} karakter girilmelidir.", too_long: "için en fazla %{count} karakter girilebilir." }
    validates :description, length: { minimum:50, maximum:5000, too_short: "için en az %{count} karakter girilmelidir.", too_long: "için en fazla %{count} karakter girilebilir." } 
    validates :origin, presence: { message: "seçilmelidir." }
    web_regex = /(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?/ix
    validates :web, format: { with: web_regex, message: "örnek: http://www.ornek.com.. veya https://www.ornek.com..." }
    validate :correct_image_type

    private

        def correct_image_type
            if image.attached? && !image.content_type.in?(%w(image/jpeg image/png image/jpg))
                errors.add(:image, " görseli .jpg/ .jpeg/ .png uzantılı olmalıdır.")
            elsif image.attached? == false
                errors.add(:image, " seçilmelidir.")
            end
        end
    
end
