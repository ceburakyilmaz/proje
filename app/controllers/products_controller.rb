class ProductsController < ApplicationController
    before_action :find_product, only: [:edit, :update, :destroy]
    before_action :select_of_trademarks, only: [:index, :new, :edit, :update]

    def index
        @products = Product.all.order("name ASC")
    end

    def new
        @product = Product.new
    end

    def create
        @product = Product.new(product_params)
        @product.trademark_id = params[:product][:trademark_id]
        if @product.save
            flash[:notice] = "ürününüz başarıyla eklenmiştir"
            redirect_to products_path
        else
            render 'new'
        end
    end

    def edit
    end
    
    def update
        @product.trademark_id = params[:trademark_id]

        if @product.update(product_params)
            redirect_to products_path
        else
            render 'edit'
        end
    end
    
    def destroy
        @product.destroy
        redirect_to products_path
    end

    private

    def product_params
        params.require(:product).permit(:name, :sector, :price, :y_o_production, :trademark_id)
    end

    def find_product
        @product = Product.find(params[:id])
    end

    def select_of_trademarks
        @trademarks = Trademark.all.map{ |t| [t.name, t.id] }
    end

end
