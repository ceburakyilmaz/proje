class TrademarksController < ApplicationController
    before_action :find_trademark, only: [:edit, :update, :show, :destroy]

    def index
        @trademarks = Trademark.all.order("name ASC")
    end

    def new
        @trademark = Trademark.new
    end

    def create
        @trademark = Trademark.new(trademark_params)
        if @trademark.save
            redirect_to @trademark
        else
            render 'new'
        end
    end

    def show
        @products = Product.where(:trademark_id => params[:id]).order("created_at DESC")
    end

    def edit
    end

    def update
        if @trademark.update(trademark_params)
            redirect_to @trademark, notice: "Güncelleme işlemi yapıldı."
        else
            render 'edit'
        end
    end

    def destroy
        @trademark.destroy
        redirect_to trademarks_path
    end

    private

        def trademark_params
            params.require(:trademark).permit(:name, :description, :origin, :web, :image)
        end

        def find_trademark
            @trademark = Trademark.find(params[:id])
        end

end
